package android.view;

public interface SubMenu extends Menu {

	public MenuItem getItem();

}
